class Student

  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    raise conflicting_courses if has_conflict?(course)
    unless @courses.include?(course)
      @courses << course
      course.students << self
    end
  end

  def has_conflict?(course)
    @courses.any? do |enrolled_course|
      course.conflicts_with?(enrolled_course)
    end
  end

  def conflicting_courses
    'Course conflicts with enrolled course'
  end

  def course_load
    student_courses = Hash.new(0)
    @courses.each do |course|
      student_courses[course.department] += course.credits
    end
    student_courses
  end

end
